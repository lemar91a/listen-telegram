import asyncio
import configparser

from telethon import TelegramClient, events
from gtts import gTTS
import pyttsx3


config = configparser.ConfigParser()
config.read("config.ini")
api_id   = config['Telegram']['api_id']
api_hash = config['Telegram']['api_hash']
my_channel_id = config['Telegram']['my_channel_id']

channels = [-1001243038268, -1001078868616, -1001482176412, -1001476594010]
set_name_channels = {'1243038268': 'Канал - Нехта', '1078868616': 'Канал - Тут Бай',
                     '1482176412': 'Канал - Конь на работе', '1476594010': 'Тестовый канал'}

client = TelegramClient('myGrab', api_id, api_hash)
print("GRAB - Started")

n = 0
@client.on(events.NewMessage(chats=channels))
async def my_event_handler(event):
    # if event.message:    # Если необходимо дополнительно передавать пост на свой канал
    #     await client.send_message(my_channel_id, event.message)

    message = event.message.to_dict()['message']
    print(message) # Проверяем, что пришел текст поста
    channel_id = str(event.message.to_dict()['peer_id']['channel_id'])
    print(channel_id) # Проверяем, что забрали id канала, от которого пришел пост
    global n
    n += 1
    for k, v in set_name_channels.items():
        if k == channel_id:
            mess = v + '.' + ' ' + message
            print(mess) # Видим в консоли название канала и текст поста

            audio = "Пост канала {name} №{number}.mp3".format(name=v, number=n)
            print(audio) # Видим в консоли форматированное название аудиофайла
            language = 'ru'
            sp = gTTS(text=mess, lang=language, slow=False)
            sp.save(audio)

            engine = pyttsx3.init()
            engine.say(mess)
            engine.setProperty('rate', 120)  # 120 words per minute
            engine.setProperty('volume', 0.9)
            engine.runAndWait()
        else:
            print("Телеграм-канал не найден")


client.start()
client.run_until_disconnected()